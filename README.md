# mobfirst-notification-history-pod

[![CI Status](http://img.shields.io/travis/Diego Merks/mobfirst-notification-history-pod.svg?style=flat)](https://travis-ci.org/Diego Merks/mobfirst-notification-history-pod)
[![Version](https://img.shields.io/cocoapods/v/mobfirst-notification-history-pod.svg?style=flat)](http://cocoapods.org/pods/mobfirst-notification-history-pod)
[![License](https://img.shields.io/cocoapods/l/mobfirst-notification-history-pod.svg?style=flat)](http://cocoapods.org/pods/mobfirst-notification-history-pod)
[![Platform](https://img.shields.io/cocoapods/p/mobfirst-notification-history-pod.svg?style=flat)](http://cocoapods.org/pods/mobfirst-notification-history-pod)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

mobfirst-notification-history-pod is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "mobfirst-notification-history-pod"
```

## Author

Diego Merks, merks@mobfirst.com

## License

mobfirst-notification-history-pod is available under the MIT license. See the LICENSE file for more info.
