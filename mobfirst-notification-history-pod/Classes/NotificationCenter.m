//
//  NotificationCenter.m
//  NotificationCenterTest
//
//  Created by Teste Eclipse 2 on 26/06/17.
//  Copyright © 2017 Teste Eclipse 2. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NotificationCenter.h"

#define NOTIFICATION_STORAGE_KEY @"NOTIFICATION_STORAGE"

#define MAX_NOTIFICATIONS_BUNDLE_KEY @"maxNotifications"
#define DEFAULT_MAX_NOTIFICATIONS_VALUE 10

@implementation NotificationCenter

+ (id)getInstance {
    
    static NotificationCenter *instance = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        instance = [[self alloc] init];
    });
    
    return instance;
}

- (id)init {
    
    self = [super init];
    if(self) {
        
        NSNumber *maxNotifications = [[NSBundle mainBundle] objectForInfoDictionaryKey:MAX_NOTIFICATIONS_BUNDLE_KEY];
        NSData *data = [[NSUserDefaults standardUserDefaults] objectForKey:NOTIFICATION_STORAGE_KEY];
        
        if(maxNotifications) {
            self.maxNotifications = [maxNotifications intValue];
        }
        else {
            self.maxNotifications = DEFAULT_MAX_NOTIFICATIONS_VALUE;
        }
        
        if(data) {
            
            NSArray *notifications = [NSKeyedUnarchiver unarchiveObjectWithData:data];
            
            if(notifications) {
                self.notifications = [NSMutableArray arrayWithArray:notifications];
            }
            else {
                self.notifications = [[NSMutableArray alloc] init];
            }
        }
        else {
            self.notifications = [[NSMutableArray alloc] init];
        }
    }
    
    return self;
}

- (BOOL)saveNotification:(Notification *) notification {
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    if(!self.notifications) {
        self.notifications = [NSMutableArray arrayWithObject:notification];
    }
    else {
        
        if([self.notifications count] >= self.maxNotifications) {
            [self.notifications removeLastObject];
        }
        
        [self.notifications addObject:notification];
    }
    
    [userDefaults removeObjectForKey:NOTIFICATION_STORAGE_KEY];
    [userDefaults setObject:[NSKeyedArchiver archivedDataWithRootObject:self.notifications] forKey:NOTIFICATION_STORAGE_KEY];
    
    return [userDefaults synchronize];
}

- (BOOL)saveNotifications:(NSArray *) notifications {
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    if(!self.notifications) {
        self.notifications = [NSMutableArray arrayWithArray:notifications];
    }
    else {
        
        for(Notification *notification in notifications) {
            
            if([self.notifications count] >= self.maxNotifications) {
                [self.notifications removeLastObject];
            }
            
            [self.notifications addObject:notification];
        }
    }
    
    [userDefaults removeObjectForKey:NOTIFICATION_STORAGE_KEY];
    [userDefaults setObject:[NSKeyedArchiver archivedDataWithRootObject:self.notifications] forKey:NOTIFICATION_STORAGE_KEY];
    
    return [userDefaults synchronize];
}

- (BOOL)deleteNotificationAtIndex:(int) index {
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    if(!self.notifications) return NO;
    if([self.notifications count] > index) {
        
        [self.notifications removeObjectAtIndex:index];
        
        [userDefaults removeObjectForKey:NOTIFICATION_STORAGE_KEY];
        [userDefaults setObject:[NSKeyedArchiver archivedDataWithRootObject:self.notifications] forKey:NOTIFICATION_STORAGE_KEY];
        
        return [userDefaults synchronize];
    }
    
    return NO;
}

- (NSArray *)getNotificationList {
    
    return self.notifications;
}

- (BOOL)deleteAllNotifications {
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    if(self.notifications) {
        [self.notifications removeAllObjects];
    }
    else {
        self.notifications = [[NSMutableArray alloc] init];
    }
    
    [userDefaults removeObjectForKey:NOTIFICATION_STORAGE_KEY];
    return [userDefaults synchronize];
}

- (unsigned long)getCount {
    
    return [self.notifications count];
}

- (void)dealloc { }

@end
