//
//  NotificationCenter.h
//  NotificationCenterTest
//
//  Created by Teste Eclipse 2 on 26/06/17.
//  Copyright © 2017 Teste Eclipse 2. All rights reserved.
//

#ifndef NotificationCenter_h
#define NotificationCenter_h

#import "Notification.h"

@interface NotificationCenter : NSObject

@property (nonatomic, strong) NSMutableArray* notifications;
@property (nonatomic) int maxNotifications;

+ (id)getInstance;

- (BOOL)saveNotification:(Notification *) notification;
- (BOOL)saveNotifications:(NSArray *) notifications;
- (BOOL)deleteNotificationAtIndex:(int) index;
- (NSArray *)getNotificationList;
- (BOOL)deleteAllNotifications;
- (unsigned long)getCount;

@end

#endif /* NotificationCenter_h */
