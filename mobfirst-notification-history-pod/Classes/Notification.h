//
//  NotificationData.h
//  NotificationCenterTest
//
//  Created by Teste Eclipse 2 on 26/06/17.
//  Copyright © 2017 Teste Eclipse 2. All rights reserved.
//

#ifndef Notification_h
#define Notification_h

@interface Notification : NSObject

@property (nonatomic, strong) NSString *longMessage;
@property (nonatomic, strong) NSString *shortMessage;
@property (nonatomic, strong) NSDate *triggerDate;

- (id)initWithLongMessage:(NSString *) longMessage shortMessage:(NSString *) shortMessage andTriggerDate:(NSDate *) triggerDate;
- (id)initWithCoder:(NSCoder *) coder;
- (void)encodeWithCoder:(NSCoder *) coder;

@end

#endif /* Notification_h */
