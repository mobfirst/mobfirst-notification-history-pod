//
//  NotificationData.m
//  NotificationCenterTest
//
//  Created by Teste Eclipse 2 on 26/06/17.
//  Copyright © 2017 Teste Eclipse 2. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Notification.h"

#define LONG_MESSAGE_KEY @"LONG_MESSAGE"
#define SHORT_MESSAGE_KEY @"SHORT_MESSAGE"
#define DATE_KEY @"DATE"

@implementation Notification

- (id)initWithLongMessage:(NSString *) longMessage shortMessage:(NSString *) shortMessage andTriggerDate:(NSDate *) triggerDate {
    
    self = [super init];
    if(self) {
        
        self.longMessage = longMessage;
        self.shortMessage = shortMessage;
        self.triggerDate = triggerDate;
    }
    
    return self;
}

- (id)initWithCoder:(NSCoder *)coder {
    
    self = [super init];
    if (self) {
        
        self.longMessage = [coder decodeObjectForKey:LONG_MESSAGE_KEY];
        self.shortMessage = [coder decodeObjectForKey:SHORT_MESSAGE_KEY];
        self.triggerDate = [coder decodeObjectForKey:DATE_KEY];
    }
    
    return self;
}

- (void)encodeWithCoder:(NSCoder *) coder {
    
    [coder encodeObject:self.longMessage forKey:LONG_MESSAGE_KEY];
    [coder encodeObject:self.shortMessage forKey:SHORT_MESSAGE_KEY];
    [coder encodeObject:self.triggerDate forKey:DATE_KEY];
}

@end
